| Original | New lyrics |
| -------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
|Une manie de vieux garçon | I was coding in a fosdem |
| Moi j'ai pris l'habitude | Only big nerds around |
| D'agrémenter ma sollitude | When I heard singing very loud |
| Aux accents de cette chanson | The new brusselian anthem |
| Quand je pense à Fernande | Quand elle code Fernande |
| Je bande, je bande | Je bande, je bande |
| Quand j'pense à Félicie | Quand code Félicie |
| Je bande aussi | Je bande aussi |
| Quand j'pense à Léonore | Quand code Léonore |
| Mon Dieu je bande encore | Mon Dieu je bande encore |
| Mais quand j'pense à Lulu | Mais quand je vois un barbu |
| Là je ne bande plus | Là je ne bande plus |
| La bandaison papa | La bandaison papa |
| Ça n'se commande pas | Ça n'se commande pas |
| C'est cette mâle ritournelle | Not understanding what I hear |
| Cette antienne virile | I catch my Belgian mate |
| Qui retentit dans la guérite | And ask him if he can translate |
| De la vaillante sentinelle | In the mother tongue of Shakespeare |
| Quand je pense à Fernande | Commit from Alison |
| Je bande, je bande | I have a hard on |
| Quand j'pense à Félicie | Commit from Marie-Lou |
| Je bande aussi | I am hard too |
| Quand j'pense à Léonore | Commit comes from Milayne |
| Mon Dieu je bande encore | My god! I'm hard again |
| Mais quand j'pense à Lulu | But bob talks of ai |
| Là je ne bande plus | I am limp, I don't know why |
| La bandaison papa | For joysticks, gentleman |
| Ça n'se commande pas | There is no sudo no man |
| Afin de tromper son cafard | Entering a room who do I see? |
| De voir la vie moins terne | The superstar Stallman! |
| Tout en veillant sur sa lanterne | Such a surprised as his organ |
| Chante ainsi le gardien de phare | Burst out into poetry |
| Quand je pense à Fernande | Commit from Alison |
| Je bande, je bande | I am a hard on |
| Quand j'pense à Félicie | Commit from Marie-Lou |
| Je bande aussi | I am hard too |
| Quand j'pense à Léonore | Commit comes from Milayne |
| Mon Dieu je bande encore | My god I am hard again |
| Mais quand j'pense à Lulu | But Bob talks of ai |
| Là je ne bande plus | I am limp I don't know why! |
| La bandaison papa | For joysticks, gentleman |
| Ça n'se commande pas | There is no sudo, no man |
| Après la prière du soir | So I started with my own project |
| Comme il est un peu triste | guidy with what they said |
| Chante ainsi le séminariste | But as time passed, I felt mislead |
| À genoux sur son reposoir | And I observe in retrospect: |
| Quand je pense à Fernande | Commit from alison |
| Je bande, je bande | A yearly hard on |
| Quand j'pense à Félicie | Commit from Mary-lou |
| Je bande aussi | Once a year too |
| Quand j'pense à Léonore | No commit from Milain |
| Mon Dieu je bande encore | My got to wait in vain? |
| Mais quand j'pense à Lulu | But Bob's talks on AI |
| Là je ne bande plus | On this there was no lie |
| La bandaison papa | And indeed gentleman |
| Ça n'se commande pas | There is no sudo, no man |
| À l'Étoile où j'étais venu | Sitting in a corner coding |
| Pour ranimer la flamme | There was she, Mari-Lou! |
| J'entendis ému jusqu'aux larmes | But when I say "hey how are you?" |
| La voix du soldat inconnu | Guess what she started to sing? |
| Quand je pense à Fernande | Commit from Salomon, |
| Je bande, je bande | I'm getting turned on |
| Quand j'pense à Félicie | Commit comes from Andrew |
| Je bande aussi | A turn on too |
| Quand j'pense à Léonore | Commit comes from Hector / Mark Twain |
| Mon Dieu je bande encore | My god, turned on once more / This one cannot explain |
| Mais quand j'pense à Lulu | But Bob talks of AI |
| Là je ne bande plus | And still we don't know why |
| La bandaison papa | On trackpoint, gentleman |
| Ça n'se commande pas | There is no sudo no man |
| Et je vais mettre un point final |  |
| À ce chant salutaire |  |
| En suggérant au solitaire |  |
| D'en faire un hymme national |  |
| Quand je pense à Fernande |  |
| Je bande, je bande |  |
| Quand j'pense à Félicie |  |
| Je bande aussi |  |
| Quand j'pense à Léonore |  |
| Mon Dieu je bande encore |  |
| Mais quand j'pense à Lulu |  |
| Là je ne bande plus |  |
| La bandaison papa |  |
| Ça n'se commande pas
